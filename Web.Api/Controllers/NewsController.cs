﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Model;
using Web.Api.Services.IServices;
using Web.Api.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly INoticiasService _noticiasService;
        public NewsController(INoticiasService noticiasService)
        {
            _noticiasService = noticiasService;
        }
        // GET: api/<NoticiasController>
        [HttpGet("search/{dateFrom}/{dateTo}/{keywords}/{country}")]
        public async Task<ResponseNewsViewModel> Search(DateTime dateFrom, DateTime dateTo, string keywords, string country)
        {
            var filterNews = new FilterNews
            {
                dateFrom = dateFrom,
                dateTo = dateTo,
                keywords = keywords,
                country = country
            };

            var resp = await _noticiasService.Search(filterNews);
            var lstOrdernada = resp.articles.OrderByDescending(x => x.publishedAt).ToList();
            resp.articles = lstOrdernada;
            resp.pageSize = 5;

            return resp;
        }

        [HttpGet("top-headlines/{country}")]
        public async Task<ResponseNewsViewModel> TopHeadlines(string country)
        {
            var resp = await _noticiasService.TopHeadlines(country);
            var lstOrdenada = resp.articles.OrderByDescending(x => x.publishedAt).ToList();
            resp.articles = lstOrdenada;

            return resp;
        }
    }
}

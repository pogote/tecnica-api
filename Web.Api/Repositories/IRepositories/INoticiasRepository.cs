﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Model;
using Web.Api.ViewModels;

namespace Web.Api.Repositories.IRepositories
{
    public interface INoticiasRepository
    {
        Task<ResponseNewsViewModel> Search(FilterNews filter);
        Task<ResponseNewsViewModel> TopHeadLines(string country);
    }
}

﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Model;
using Web.Api.Repositories.IRepositories;
using Web.Api.ViewModels;

namespace Web.Api.Repositories
{
    public class NoticiasRepository: INoticiasRepository
    {
        private IConfiguration _configuration;

        public NoticiasRepository(IConfiguration configuration) 
        {
            _configuration = configuration;
        }
        public async Task<ResponseNewsViewModel> Search(FilterNews filter)
        {
            var dateFrom = filter.dateFrom.ToString("yyyy-MM-dd");
            var dateTo = filter.dateTo.ToString("yyyy-MM-dd");
            var client = new RestClient("https://newsapi.org/");
            var request = new RestRequest("v2/everything/", Method.GET);
            request.AddParameter("q", filter.keywords);
            request.AddParameter("dateFrom", dateFrom);
            request.AddParameter("dateTo", dateTo);
            request.AddParameter("language", filter.country);
            request.AddHeader("x-api-key", _configuration.GetValue<string>("ApiKey:x-api-key"));
            var result = await client.ExecuteAsync(request);
            return JsonConvert.DeserializeObject<ResponseNewsViewModel>(result.Content);
        }

        public async Task<ResponseNewsViewModel> TopHeadLines(string country)
        {
            var client = new RestClient("https://newsapi.org/");
            var request = new RestRequest("v2/top-headlines/", Method.GET);
            request.AddParameter("country", country);
            request.AddHeader("x-api-key", _configuration.GetValue<string>("ApiKey:x-api-key"));
            var result = await client.ExecuteAsync(request);
            return JsonConvert.DeserializeObject<ResponseNewsViewModel>(result.Content);
        }
    }
}

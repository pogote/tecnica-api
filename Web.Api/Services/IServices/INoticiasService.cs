﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Model;
using Web.Api.ViewModels;

namespace Web.Api.Services.IServices
{
    public interface INoticiasService
    {
        Task<ResponseNewsViewModel> Search(FilterNews filter);
        Task<ResponseNewsViewModel> TopHeadlines(string country);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Model;
using Web.Api.Repositories.IRepositories;
using Web.Api.Services.IServices;
using Web.Api.ViewModels;

namespace Web.Api.Services
{
    public class NoticiasService : INoticiasService
    {
        private readonly INoticiasRepository _noticiasRepository;
        public NoticiasService(INoticiasRepository noticiasRepository)
        {
            _noticiasRepository = noticiasRepository;
        }
        public async Task<ResponseNewsViewModel> Search(FilterNews filter)
        {
            return await _noticiasRepository.Search(filter);
        }

        public async Task<ResponseNewsViewModel> TopHeadlines(string country)
        {
            return await _noticiasRepository.TopHeadLines(country);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Api.Model
{
    public class FilterNews
    {
        public string q { get; set; }
        public string qInTitle { get; set; }
        public string sources { get; set; }
        public string domains { get; set; }
        public string excludeDomains { get; set; }
        public string language { get; set; }
        public string sortBy { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public string keywords { get; set; }
        public string country { get; set; }
        public int page { get; set; }
        public int pageSize { get; set; }
    }

    public class ParametrosBusqueda
    {
        public string q { get; set; }
        public string country { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public int pageSize { get; set; }
        public int page { get; set; }
    }
}

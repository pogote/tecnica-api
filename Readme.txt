Instrucciones para levantar api

- Dirigirse a la siguiente url httpsbitbucket.orgpogotetecnica-apisrcmaster
- Dar clic en boton Clone que se encuentra en la esquina superior derecha.
- En el popup que se abrirá seleccionar clonar en sourcetree (tener en cuenta que para usar esta opción, es necesario tener el sourcetree descargado con anterioridad).
- Cuando se abra el sourcetree, seleccionar la carpeta en la cual queremos clonar el proyecto subido.
- Finalmente dirigirse a la carpeta, clic derecho sobre WebApi.sln, iniciar con visual studio 2019.
- Una vez que se abrió el visual estudio, darle clic a iniciar (con esto ya tendremos levantada la api de nuestro portal).